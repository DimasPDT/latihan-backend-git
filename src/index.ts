import express, { Request, Response } from 'express';

const app = express();
const port = 3000;

app.use(express.json());

interface User {
    username: string;
    password: string;
}

interface Product {
    id: number;
    name: string;
    price: number;
}

interface Order {
    id: number;
    productId: number;
    quantity: number;
}

let users: User[] = [];
let products: Product[] = [];
let orders: Order[] = [];

app.post('/register', (req: Request, res: Response) => {
    const { username, password } = req.body;
    if (!username || !password) {
        return res.status(400).send('Username and password are required');
    }

    const existingUser = users.find(user => user.username === username);
    if (existingUser) {
        return res.status(400).send('Username already exists');
    }

    users.push({ username, password });
    res.send('User registered');
});

app.post('/login', (req: Request, res: Response) => {
    const { username, password } = req.body;
    if (!username || !password) {
        return res.status(400).send('Username and password are required');
    }

    const user = users.find(user => user.username === username && user.password === password);
    if (!user) {
        return res.status(400).send('Invalid username or password');
    }

    res.send('User logged in');
});

app.post('/products', (req: Request, res: Response) => {
    const { name, price } = req.body;
    if (!name || !price) {
        return res.status(400).send('Name and price are required');
    }

    const id = products.length + 1;
    products.push({ id, name, price });
    res.send('Product added');
});

app.get('/products', (req: Request, res: Response) => {
    res.json(products);
});

app.post('/orders', (req: Request, res: Response) => {
    const { productId, quantity } = req.body;
    if (!productId || !quantity) {
        return res.status(400).send('Product ID and quantity are required');
    }

    const product = products.find(p => p.id === productId);
    if (!product) {
        return res.status(400).send('Product not found');
    }

    const id = orders.length + 1;
    orders.push({ id, productId, quantity });
    res.send('Order created');
});

app.get('/orders', (req: Request, res: Response) => {
    res.json(orders);
});

app.listen(port, () => {
    console.log(`Server running on port ${port}`);
});
